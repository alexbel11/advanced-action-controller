<?php

namespace AdvancedActionController\Controller;
use Zend\Mvc\Controller\AbstractActionController as ActionController;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

/**
 * Class AbstractActionController
 * @package AdvancedActionController\Controller
 */
class AbstractActionController extends ActionController {

    /**
     * The View Model
     * @var ViewModel
     */
    protected $view;

    /**
     * Constructor - Initialize the view model
     */
    public function __construct()
    {
        $this->view = new ViewModel();
    }

    /**
     * On Dispatch Event
     * @param MvcEvent $e
     * @return mixed|void
     */
    public function onDispatch(MvcEvent $e)
    {
        $this->view->setVariables($e->getViewModel()->getVariables());
        $result = parent::onDispatch($e);
        $e->getResponse()->getHeaders()->addHeaderLine('Access-Control-Allow-Origin', '*');
        return $result;
    }

    /**
     * Tests if the request is a post or not
     * @return boolean
     */
    public function isPost()
    {
        return $this->getRequest()->isPost();
    }

    /**
     * Gets the posted data
     * @param string|null $key
     * @return array
     */
    public function getPost($key = null)
    {
        $post = $this->getRequest()->getPost();

        if( $key === null )
        {
            return $post;
        }

        return $post[$key];
    }

    /**
     * Gets the posted files
     * @return array
     */
    public function getFiles()
    {
        return $this->getRequest()->getFiles();
    }

    /**
     * Gets the configuration array
     * @return array|object
     */
    public function getConfig()
    {
        return $this->getServiceLocator()->get('Config');
    }

    /**
     * Gets the URL params
     * @return array
     */
    public function getParams()
    {
        return $this->getEvent()->getRouteMatch()->getParams();
    }

    /**
     * Gets a parameter
     * @param $param
     * @return mixed
     */
    public function getParam($param)
    {
        $params = $this->getParams();

        if( ! isset($params[$param]))
        {
            return null;
        }

        return $params[$param];
    }

    /**
     * Gets the route match object
     * @return mixed
     */
    public function getRouteMatch()
    {
        return $this->event->getRouteMatch();
    }

    /**
     * Sets the page title
     * @param $title
     */
    public function setPageTitle($title)
    {
        $this->getServiceLocator()->get('viewhelpermanager')->get('headTitle')->set($title);
    }

    /**
     * Append the page title
     * @param $title
     * @param $separator
     */
    public function appendPageTitle($title, $separator = ' - ')
    {
        $helper = $this->getServiceLocator()->get('viewhelpermanager')->get('headTitle');
        $helper->setSeparator($separator);
        $helper->append($title);
    }

    /**
     * Gets the name of the module
     * @return string
     */
    public function getModuleName()
    {
        $parts = $this->getEvent()->getRouteMatch()->getParams();
        return strtolower(array_shift(explode('\\',$parts['controller'])));
    }

    /**
     * Gets the name of the controller
     * @return string
     */
    public function getControllerName()
    {
        $parts = $this->getEvent()->getRouteMatch()->getParams();
        return strtolower(array_pop(explode('\\',$parts['controller'])));
    }

    /**
     * Gets the name of the controller
     * @return string
     */
    public function getActionName()
    {
        $parts = $this->getEvent()->getRouteMatch()->getParams();
        return strtolower($parts['action']);
    }
}